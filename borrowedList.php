<?php require("haut.php"); ?>

<section>
<h3>Liste des emprunts en cours</h3>
<p style="text-align:center">Trier par : <a href="borrowedList.php?tri=date_emprunt">date d'emprunt</a> - <a href="borrowedList.php?tri=date_retour">date de retour prévue</a> - <a href="borrowedList.php?tri=id_doc">ID du document</a> - <a href="borrowedList.php?tri=id_user">ID de l'utilisateur</a></p>
	<table class="MonTableau">
		<tr class="index">
			<th>Date d'emprunt</th>
			<th>Date de retour prévue</th>
			<th>ID du document</th>
            <th>ID de l'utilisateur</th>
		</tr>
<?php 
	if (!empty($_GET["tri"])){
		$req = mysqli_query($db, "SELECT * FROM emprunts ORDER BY " . $_GET["tri"] . " asc");
	} else {
		$req = mysqli_query($db, "SELECT * FROM emprunts ORDER BY date_emprunt ASC");
	}
	while ($row = mysqli_fetch_row($req))
	{
?>
		<tr class="light">
			<td><?php echo $row['0']; ?></td>
			<td><?php echo $row['1']; ?></td>
			<td><?php echo $row['2']; ?></td>
			<td><?php echo $row['3']; ?></td>
		</tr>	
<?php
	}
?>
	</table>
</section>

<?php require("bas.php"); ?>