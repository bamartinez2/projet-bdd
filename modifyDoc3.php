<?php require("haut.php"); ?>

<section id="main">
	<h4>Modification d'un document</h4>

<?php
try{
	$sql='UPDATE documents SET id_doc=?, titre=?, auteurs=?, type_doc=?, annee_parution=? WHERE id_doc=?';
	$req = mysqli_prepare($db, $sql) or die(mysqli_error($db));
	mysqli_stmt_bind_param($req, 'isssii', $id, $title, $auth, $type, $year, $old);
	$id=$_POST["n_id_doc"];
	$title=$_POST["n_titre"];
	$auth=$_POST["n_auteurs"];
	$year=$_POST["n_annee_parution"];
	$type=$_POST["n_type_doc"];
	$old=$_POST["old_id_doc"];
    mysqli_stmt_execute($req);
	?>
    <div class="alert alert-success"><p>La modification a bien &eacute;t&eacute; enregistr&eacute;e.</p></div>
<?php }catch (Exception $e){
	die('Erreur : ' . $e->getMessage());
?>
	<div class="alert alert-danger"><p>Une erreur est survenue lors de la modification.</p></div>
<?php 
}
?>

</section>

<?php require("bas.php"); ?>