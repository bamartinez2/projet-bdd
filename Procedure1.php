<?php require("haut.php"); ?>

<div class="form-center">
<p>Que souhaitez-vous faire ?</p>
<form role="form" method="post" action="Procedure2.php">
	<label>
    	<input type="radio" name="boutonRadio" id="boutonRadio1" value="0">
        	Créer une brèche dans la sécurité, mettre tous les niveaux de confidentialité à 0
    </label>
</div>
<div class="form-center">
    <label>
    	<input type="radio" name="boutonRadio" id="boutonRadio2" value="1">
        	Montrer qui est le patron, mettre tout à 1
    </label>
    <br/>
    <br/>
    <button type="submit" class="btn btn-default btn-perso">Envoyer</button>
</form>
</div>
<?php require("bas.php"); ?>