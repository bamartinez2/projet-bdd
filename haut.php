<!DOCTYPE html>
<html lang="fr">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Site web muni d'une base de donnée, destiné au projet de BDD de deuxième année filière 4">
    <meta name="author" content="Baptiste Martinez">

    <title>Centradoc BatiBaptiste</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>

    <!-- Custom styles for this template -->
    <link href="css/clean-blog.min.css" rel="stylesheet">
    <link href="css/stylesPersos.css" rel="stylesheet">

  </head>

  <body> 
  <?php
try{
	// j'ai préféré utiliser mysqli plutôt qu'une instance PDO
	$db = mysqli_connect("localhost", "root", "", "projetbaptiste");
	mysqli_set_charset($db, "utf8");
	if (mysqli_connect_errno())
	{
		echo "Failed to connect to MySQL: " . mysqli_connect_error();
	}
}catch(Exception $e){
	die('Erreur : ' . $e->getMessage());
}
?>

    <!-- Page Header -->
    <header class="masthead" style="background-image: url('img/home-bg.jpg')">
      <div class="overlay"></div>
      <div class="container">
        <div class="row">
          <div class="col-lg-8 col-md-10 mx-auto">
            <div class="site-heading">
              <h1>Centradoc BatiBaptiste</h1>
              <span class="subheading">Le centre de documentation technique de BatiBaptiste</span>
            </div>
          </div>
        </div>
      </div>
    </header>
	
        <!-- Navigation -->
    	<nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
      	<div class="container">
        <a class="navbar-brand" href="index.php">Accueil</a>
        <div class="content">
        <span style="font-size:10px">Recherche rapide par titre de document</span>
<form method="post" action="search.php">
    <select name="submitSearch">
    <?php
        $sql = mysqli_query($db, "SELECT titre FROM documents ORDER BY titre ASC");
        while ($row = mysqli_fetch_array($sql)){
    ?>
            <option>
			<?php echo $row["titre"]; ?>
            </option>
    <?php
        }
    ?>
    </select>
    <button type="submit" class="btn btn-link btn-couleur">Envoyer</button>
</form>

<?php
	if (isset($_POST["submitSearch"]))
	{
		$_POST["submitSearch"];
	}
?>
</div>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          Menu
          <i class="fa fa-bars"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
			<ul class="navbar-nav ml-auto">
				<li class="nav-item">
      				<a class="nav-link" href="index.php">Accueil</a>
      			</li>
      			<li class="nav-item">
					<a class="nav-link" href="registeredUsers.php">Utilisateurs</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="documents.php">Documents</a>
				</li>
                <li class="nav-item">
					<a class="nav-link" href="borrowedList.php">Emprunts en cours</a>
				</li>
			</ul>
		</nav>
        </div>