<?php require("haut.php"); ?>

<section>
<h3>Liste des utilisateurs inscrits</h3>
<p style="text-align:center">Trier par : <a href="registeredUsers.php?tri=id_user">id</a> - <a href="registeredUsers.php?tri=nom">nom</a> - <a href="registeredUsers.php?tri=prenom">prenom</a> - <a href="registeredUsers.php?tri=confidentialite">niveau d'autorisation</a></p>
	<table class="MonTableau">
		<tr class="index">
			<th>Id</th>
			<th>Nom</th>
			<th>Prénom</th>
            <th>Niveau de confidentialité</th>
		</tr>
<?php 
	if (!empty($_GET["tri"])){
		$req = mysqli_query($db, "SELECT * FROM utilisateurs ORDER BY " . $_GET["tri"] . " asc");
	} else {
		$req = mysqli_query($db, "SELECT * FROM utilisateurs");
	}
	while ($row = mysqli_fetch_row($req))
	{
?>
		<tr class="light">
			<td><?php echo $row['0']; ?></td>
			<td><?php echo $row['1']; ?></td>
			<td><?php echo $row['2']; ?></td>
			<td><?php echo $row['3']; ?></td>
		</tr>	
<?php
	}
?>
	</table>
</section>

<?php require("bas.php"); ?>