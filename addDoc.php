<?php require("haut.php"); ?>

<script>
// Fonction de surlignage du champ contenant le niveau de confidentialité en cas d'erreur de saisie
/*function surligne(champ, erreur)
{
   if(erreur)
      champ.style.background = "#B50012";
   else
      champ.style.background = "";
};

// Fonction vérifiant si le niveau de confidentialité a été correctement saisi
function checkConf(champ)
{
//var confid = document.getElementById("conf");
 
  //if(isNaN(confid) || (confid.value != 0 && confid.value != 1))
  if (champ.value != 0 && champ.value != 1)
  {
    //document.getElementById("alerte").innerHTML="Tapez un niveau de sécurité conforme.";
	surligne(champ, true);
	return false;
  }
  else
  {
    //document.getElementById("alerte").innerHTML="Validé";
	surligne(champ, false);
	return true;
  }
}*/
document.getElementById("conf").onchange=validateField;
function validateField() {
  if (this.value != 0 || this.value !=1) {
     this.parentNode.setAttribute("style",
"background-color: #ffcccc");
     this.setAttribute("aria-invalid", "true");
     generateAlert("Erreur de saisie - Le niveau de confidentialité doit être à 0 ou à 1.");
  }
}
</script>

<div class="content">
        <h4>Ajout d'un document</h4>

<div class="form-center">
<form method="post" action="addDoc2.php">
	ID :</br>
    <input type="text" name="n_id_doc" class="form-control col-lg-2 col-md-2 col-sm-2" value="" /></br>
    
    Intitulé :</br>
    <input type="text" name="n_titre" class="form-control col-lg-2 col-md-3 col-sm-4" value="" /></br>
    
    Auteur(s):</br>
    <input type="text" name="n_auteurs" class="form-control col-lg-2 col-md-3 col-sm-4" value="" /></br>
    
    Type de document :</br>
    <input type="text" name="n_type_doc" class="form-control col-lg-2 col-md-3 col-sm-4" value="" /></br>
    
    Année de la publication :</br>
    <input type="text" name="n_annee_parution" class="form-control col-lg-2 col-md-2 col-sm-2" value="" /></br>
    
    Niveau de confidentialité:</br>
    <input type="text" name="conf" id="conf" class="form-control col-lg-1 col-md-1 col-sm-1" value="" /></br>
    
    </br>
    <button type="submit" class="btn btn-default btn-perso">Envoyer</button>
</form>
</div>
</div>

<?php require("bas.php"); ?>