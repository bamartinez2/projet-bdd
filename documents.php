<?php require("haut.php"); ?>

<section>
<h3>Liste des documents stockés dans la bibliothèque</h3>
<p style="text-align:center">Trier par : <a href="documents.php?tri=id_doc">id</a> - <a href="documents.php?tri=titre">titre</a> - <a href="documents.php?tri=auteurs">auteur</a> - <a href="documents.php?tri=type_doc">type</a> - <a href="documents.php?tri=confidentialite">confidentialité</a> - <a href="documents.php?tri=annee_parution">année de parution</a></p>
	<table class="MonTableau">
		<tr class="index">
			<th>Id</th>
			<th>Titre</th>
			<th>Auteur(s)</th>
            <th>Type de document</th>
            <th>Année de parution</th>
            <th>Niveau de confidentialité</th>
		</tr>
<?php 
	if (!empty($_GET["tri"])){
		$req = mysqli_query($db, "SELECT * FROM documents ORDER BY " . $_GET["tri"] . " asc");
	} else {
		$req = mysqli_query($db, "SELECT * FROM documents ORDER BY TITRE ASC");
	}
	while ($row = mysqli_fetch_row($req))
	{
?>
		<tr class="light">
			<td><?php echo $row['0']; ?></td>
			<td><?php echo $row['1']; ?></td>
			<td><?php echo $row['2']; ?></td>
			<td><?php echo $row['3']; ?></td>
            <td><?php echo $row['4']; ?></td>
            <td><?php echo $row['5']; ?></td>
		</tr>	
<?php
	}
?>
	</table>
</section>

<?php require("bas.php"); ?>