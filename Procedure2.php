<?php require("haut.php"); ?>

<div class="form-center">
<h4>La révolution est en marche !</h4>

<?php
try{
	$choix=$_POST['boutonRadio'];
	$sql ='CALL REVOLUTION(?);';
	$req = mysqli_prepare($db, $sql) or die(mysqli_error($db));
	mysqli_stmt_bind_param($req, 'i', $choix);
	mysqli_stmt_execute($req);
	
	echo 'La modification a bien &eacute;t&eacute; enregistr&eacute;e. Tous les niveaux de confidentialit&eacute; des documents ont été mis à ' . $choix . '.';
}catch (Exception $e){
	die('Erreur : ' . $e->getMessage());
}
?>
<br/>
<br/>
</div>

<?php require("bas.php"); ?>