<?php require("haut.php"); ?>

<section id="main">	
	<h2>Nombre d'utilisateurs inscrits sur notre bibliothèque</h2>
		
<?php
try{
	$req = $db->query('SELECT COUNT(*) AS NB FROM UTILISATEURS');
	$req2 = $db->query('SELECT count(id_user) AS nbconf FROM utilisateurs WHERE confidentialite=1');
	$user = $req->fetch_assoc();
	$userconf = $req2->fetch_assoc();
	
	echo "La base de donn&eacute;es compte " . $user['NB'] . " utilisateurs au " .  date('d/m/Y') . ".<br>";
	
	if ($userconf['nbconf']==1) // phrase au singulier
	{
		echo "Seulement un seul utilisateur possède un niveau d'autorisation assez élevé pour lire des documents confidentiels.";
	}else if ($userconf['nbconf']==0){ //cas où il n'y a pas d'utilisateur avec la confidentialité à 1
		echo "Il n'y a aucun utilisateur possédant les droits nécessaires pour lire des documents confidentiels.";
	}else // phrase au pluriel
	{
		echo "Il y a " . $userconf['nbconf'] . " utilisateurs possédant un niveau d'autorisation assez élevé pour lire des documents confidentiels.";
	}
	
	echo "<br>";
	echo "En cas de dysfonctionnement du système de confidentialité, veuillez nous contacter.";
}catch (Exception $e){
	die('Erreur : ' . $e->getMessage());
}
?>

</section>

<?php require("bas.php"); ?>