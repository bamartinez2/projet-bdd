<?php require("haut.php"); ?>

<div class="content">
	<h4>Ajout d'un document</h4>
    <?php 
		try{
		$sql='INSERT INTO `DOCUMENTS` (id_doc, titre, auteurs, type_doc, annee_parution, confidentialite) VALUES(?, ?, ?, ?, ?, ?)';
		$req = mysqli_prepare($db, $sql) or die(mysqli_error($db));
		mysqli_stmt_bind_param($req, 'isssii', $id, $title, $auth, $type, $year, $conf);
		$id=$_POST["n_id_doc"];
		$title=$_POST["n_titre"];
		$auth=$_POST["n_auteurs"];
		$year=$_POST["n_annee_parution"];
		$type=$_POST["n_type_doc"];
		$conf=$_POST["conf"];
		mysqli_stmt_execute($req);
		?>
		<div class="alert alert-success"><p>Le document a été correctement inséré dans la base de données.</p></div>
	<?php }catch (Exception $e){
		die('Erreur : ' . $e->getMessage());
	?>
		<div class="alert alert-danger"><p>Une erreur est survenue lors de l'insertion.</p></div>
	<?php 
	}
	?>
</div>

<?php require("bas.php"); ?>