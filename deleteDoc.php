<?php require("haut.php"); ?>

<div class="content">
        <h4>Sélection de l'ID du document à supprimer</h4>
<form method="post" action="deleteDoc2.php">
    <select name="v_id_doc">
    <?php
	try{
        $sql = mysqli_query($db, "SELECT id_doc FROM documents ORDER BY id_doc ASC");
        while ($row = mysqli_fetch_array($sql)){
    ?>
            <option>
			<?php echo $row["id_doc"]; ?>
            </option>
    <?php
        }
	}catch (Exception $e){
		die('Erreur : ' . $e->getMessage());
	}
    ?>
    </select>
    <br/>
    <br/>
    <button type="submit" class="btn btn-default btn-perso">Envoyer</button>
</form>

<?php
	if (isset($_POST["v_id_doc"]))
	{
		$_POST["v_id_doc"];
	}
?>
</div>

<?php require("bas.php"); ?>