<?php require("haut.php"); ?>
    
    <section id="main">
        <!--<h2>Certaines actions exigent que vous vous identifiez au préalable.</h2>-->
        <div id="page_menu">
        <ul id="menu">
            <li><a href="addDoc.php">Ajouter un document</a></li>
            <li><a href="modifyDoc.php">Modifier un document</a></li>
            <li><a href="deleteDoc.php">Supprimer un document</a></li>
            <li><a href="number_users.php">Nombre d'utilisateurs inscrits</a></li>
            <li><a href="Procedure1.php">Lancer une procédure, mener une révolution !</a></li>
            <li><a href="Fonction.php">Lancer une fonction, voir combien d'emprunts sont en cours</a></li>
        </ul>
        </div>
    </section>
    <!-- Main Content -->
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
          <div class="post-preview">
            <a href="#">
              <h2 class="post-title">
              Signature d'un contrat avec Vinci
              </h2>
              <h3 class="post-subtitle">
              À nouveau un contrat dans la cour des grands !
              </h3>
            </a>
            <p class="post-meta">Posté par
              <a href="#">Baptiste</a>
              le 23 mars 2018</p>
          </div>
          <hr>
          <div class="post-preview">
            <a href="#">
              <h2 class="post-title">
              Le projet innovation avance
              </h2>
              <h3 class="post-subtitle">
              Les plans du projet innovation sont disponibles.
              </h3>
            </a>
            <p class="post-meta">Posté par
              <a href="#">Baptiste</a>
              le 5 mars 2018</p>
          </div>
          <hr>
          <!-- Pager -->
          <div class="clearfix">
            <a class="btn btn-primary float-right" href="#">Nouvelles plus anciennes &rarr;</a>
          </div>
        </div>
      </div>
    </div>

    <hr>

<?php require("bas.php"); ?>
