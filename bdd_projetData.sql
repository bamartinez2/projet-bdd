CREATE DATABASE Projetbaptiste;
USE Projetbaptiste;

-- Suppression des tables
DROP TABLE IF EXISTS DOCUMENTS;
DROP TABLE IF EXISTS UTILISATEURS;
DROP TABLE IF EXISTS EMPRUNTS;

-- Structure de la table `UTILISATEURS`
CREATE TABLE UTILISATEURS (
  ID_USER int(5),
  NOM varchar(40) NOT NULL,
  PRENOM varchar(60) NOT NULL,
  CONFIDENTIALITE int(1) NOT NULL, 
  PRIMARY KEY (ID_USER)
);

-- Structure de la table `DOCUMENTS`
CREATE TABLE DOCUMENTS (
  ID_DOC int(5),
  TITRE varchar(40) UNIQUE NOT NULL,
  AUTEURS varchar(100) NOT NULL DEFAULT 'Anonyme',
  TYPE_DOC varchar(20) NOT NULL,
  ANNEE_PARUTION int(4) NULL,
  CONFIDENTIALITE int(1) NOT NULL,
  PRIMARY KEY (ID_DOC)
);

-- Structure de la table `EMPRUNTS`
CREATE TABLE EMPRUNTS (
  DATE_EMPRUNT date NOT NULL,
  DATE_RETOUR date NOT NULL,
  ID_DOC int(5) NOT NULL,
  ID_USER int(5) NOT NULL,
  PRIMARY KEY (ID_DOC,DATE_EMPRUNT),
  FOREIGN KEY (ID_DOC) REFERENCES DOCUMENTS (ID_DOC),
  FOREIGN KEY (ID_USER) REFERENCES UTILISATEURS (ID_USER)
);

-- TRIGGER
DELIMITER //
CREATE TRIGGER VERIF_VALIDITE_EMPRUNT
BEFORE INSERT ON EMPRUNTS FOR EACH ROW
BEGIN
	DECLARE temps_emprunt int;
	SET temps_emprunt=(SELECT TIMESTAMPDIFF(day, NEW.DATE_EMPRUNT, NEW.DATE_RETOUR));
	IF temps_emprunt > 60 THEN
	SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Emprunt impossible, veuillez avancer la date de retour.';
	END IF;
END//
DELIMITER ;

-- Procédure REVOLUTION
DELIMITER //
CREATE PROCEDURE REVOLUTION(choix INT(1))
BEGIN
  UPDATE DOCUMENTS SET CONFIDENTIALITE = choix;
END//
DELIMITER ;

-- Fonction NBEMPRUNTS
DELIMITER //
CREATE FUNCTION NBEMPRUNTS()
  RETURNS VARCHAR(100)
BEGIN
  DECLARE v_nb_emp INT(6);
  DECLARE v_date DATE DEFAULT CURDATE();
  SET v_nb_emp=(SELECT COUNT(*) FROM EMPRUNTS);
  RETURN v_nb_emp;
END//
DELIMITER ;

-- Insertion dans la table UTILISATEURS
INSERT INTO `UTILISATEURS` VALUES('01234', 'Martinez', 'Baptiste', 0);
INSERT INTO `UTILISATEURS` VALUES('12345', 'Martinez', 'Raphaël', 1);

-- Insertion dans la table DOCUMENTS
INSERT INTO `DOCUMENTS` VALUES('1111', 'Les contes de Perrault', 'Charles Perrault', 'Livre', 1890, 0);
INSERT INTO `DOCUMENTS` VALUES('22222', 'Fusion de l\'entreprise', 'Michelin Total', 'Contrat', 2017, 1);

-- Insertion dans la table EMPRUNTS
INSERT INTO `EMPRUNTS` VALUES('2018-1-21', '2018-2-28', '1111', '01234');
INSERT INTO `EMPRUNTS` VALUES('2018-1-24', '2018-3-24', '22222', '12345');

COMMIT;