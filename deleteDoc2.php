<?php require("haut.php"); ?>

<section id="main">
	<h2>Suppression d'un document</h2>

<?php
try{
	$sql='DELETE FROM documents WHERE id_doc=?';
	$req = mysqli_prepare($db, $sql) or die(mysqli_error($db));
	mysqli_stmt_bind_param($req, 'i', $id);
	$id=$_POST["v_id_doc"];
    mysqli_stmt_execute($req);
	?>
    <div class="alert alert-success"><p>La modification a bien &eacute;t&eacute; enregistr&eacute;e.</p></div>
<?php }catch (Exception $e){
	die('Erreur : ' . $e->getMessage());
?>
	<div class="alert alert-danger"><p>Une erreur est survenue lors de la modification.</p></div>
<?php 
}
?>

</section>

<?php require("bas.php"); ?>