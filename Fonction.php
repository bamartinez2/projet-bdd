<?php require("haut.php"); ?>

<div class="form-center">
<h4>Nombre d'emprunts en cours</h4>

<?php
try{
	$req ='SELECT NBEMPRUNTS() as nb;';
	$nb = mysqli_query($db, $req);
	$result=mysqli_fetch_assoc($nb);
	echo 'Il y a ' . $result['nb'] . ' emprunts en cours au ' . date('d/m/Y') . '.';
}catch (Exception $e){
	die('Erreur : ' . $e->getMessage());
}
?>
<br/>
<br/>
</div>

<?php require("bas.php"); ?>