<?php require("haut.php"); ?>

<div class="content">
        <h4>Modification du document: <?php echo htmlspecialchars($_POST["v_id_doc"]); ?></h4>

<div class="form-center">
<form method="post" action="modifyDoc3.php">
    <?php
	try{
        $sql = mysqli_query($db, 'SELECT id_doc, titre, auteurs, type_doc, annee_parution, confidentialite FROM documents WHERE id_doc=' . htmlspecialchars($_POST["v_id_doc"]) . ';');
		$doc = mysqli_fetch_array($sql);
	}catch (Exception $e){
		die('Erreur : ' . $e->getMessage());
	}
    ?>
    <input type="hidden" name="old_id_doc" value="<?php echo htmlspecialchars($_POST['v_id_doc']); ?>" />
    ID du document :</br>
    <input type="text" name="n_id_doc" class="form-control col-lg-2 col-md-2 col-sm-2" value="<?php echo $doc["id_doc"]; ?>" /></br>
    
    Intitulé :</br>
    <input type="text" name="n_titre" class="form-control col-lg-2 col-md-3 col-sm-4" value="<?php echo $doc["titre"]; ?>" /></br>
    
    Auteur(s):</br>
    <input type="text" name="n_auteurs" class="form-control col-lg-2 col-md-3 col-sm-4" value="<?php echo $doc["auteurs"]; ?>" /></br>
    
    Type de document :</br>
    <input type="text" name="n_type_doc" class="form-control col-lg-2 col-md-3 col-sm-4" value="<?php echo $doc["type_doc"]; ?>" /></br>
    
    Année de la publication :</br>
    <input type="text" name="n_annee_parution" class="form-control col-lg-2 col-md-2 col-sm-2" value="<?php echo $doc["annee_parution"]; ?>" /></br>
    
    Niveau de confidentialité (non modifiable):</br>
    <input type="text" name="conf" class="form-control col-lg-1 col-md-1 col-sm-1" disabled readonly value="<?php echo $doc["confidentialite"]; ?>" /></br>
    
    </br>
    <button type="submit" class="btn btn-default btn-perso">Envoyer</button>
</form>
</div>
</div>

<?php require("bas.php"); ?>